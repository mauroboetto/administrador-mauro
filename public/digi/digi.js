

function convertDecklist() {
    let jsonText = document.getElementById('input-decklist').value;
    console.log(jsonText);
    let cardsList = JSON.parse(jsonText);
    let counters = {};
    for (let card of cardsList) {
        if (typeof card !== 'string') {
            continue;
        }
        card = card.trim();
        if (card.startsWith('Exported from')) {
            continue;
        }
        if (counters[card]) {
            counters[card]++;
        } else {
            counters[card] = 1;
        }
    }

    let converted = '';
    for (let card in counters) {
        converted += counters[card].toString() + ' ' + card + '\n';
    }
    document.getElementById('output-decklist').value = converted;
}