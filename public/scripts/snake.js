
$(document).ready(startSnake);

function RandInt(min, max) {
	return min + Math.floor(Math.random() * (max + 1 - min));
}


function startSnake() {
	let ctrl = new SnakeController(20, 10, 500);
	ctrl.mainLoop();

	$('#stop-snake-btn').click(function() {
		ctrl.stop();
	});
	$('#snake-btn-left').click(function() {
		ctrl.turnLeft();
	});
	$('#snake-btn-right').click(function() {
		ctrl.turnRight();
	});
}

// ------------------
const STATE_STARTING = 0;
const STATE_PLAYING = 1;
const STATE_STOPPED = 2;
// ------------------
const SNAKE_DIR_UP = 0;
const SNAKE_DIR_DOWN = 1;
const SNAKE_DIR_LEFT = 2;
const SNAKE_DIR_RIGHT = 3;
// ------------------
const SNAKE_NO_DIR_TURN = 0;
const SNAKE_TURN_LEFT = 1;
const SNAKE_TURN_RIGHT = 2;

class SnakeController {
	constructor(screenWidth, screenHeight, speed) {
		this.screenWidth = screenWidth;
		this.screenHeight = screenHeight;
		this.speed = speed;
		this.screenMatrix = new Array(this.screenHeight);
		for (let i = 0; i < this.screenMatrix.length; i++) {
			this.screenMatrix[i] = new Array(this.screenWidth);
			for (let j = 0; j < this.screenMatrix[i].length; j++) {
				this.screenMatrix[i][j] = 0;
			}
		}

		this.state = STATE_STARTING;
		this.tailTile = null;
		this.snakeHead_x = null;
		this.snakeHead_y = null;
		this.snakeHeadNext_x = null;
		this.snakeHeadNext_y = null;
		this.snakeDirection = null;
		this.snakeTurn = null;
	}

	mainLoop() {
		if (!this.stopped()) {
			this.updateMatrix();
			this.drawScreen();

			let ctrl = this;
			setTimeout(function() {
				ctrl.mainLoop();
			}, this.speed);
		}
	}

	stop() {
		this.state = STATE_STOPPED;
		console.log('stop');
		console.log(this.screenMatrix);
	}
	stopped() {
		return this.state === STATE_STOPPED;
	}

	initializeMatrix() {
		let center_x = Math.floor(this.screenWidth / 2);
		let center_y = Math.floor(this.screenHeight / 2);

		this.screenMatrix[center_y][center_x - 1] = 1;
		this.screenMatrix[center_y][center_x] = 2;
		this.screenMatrix[center_y][center_x + 1] = 3;
		this.tailTile = 3;

		this.snakeHead_x = center_x -1;
		this.snakeHead_y = center_y;

		this.state = STATE_PLAYING;
		this.snakeDirection = SNAKE_DIR_LEFT;
		this.snakeTurn = SNAKE_NO_DIR_TURN;
	}

	updateMatrix() {
		if (this.state === STATE_STARTING) {
			this.initializeMatrix();
		} else {
			this.updateDirection();
			this.calcNextTile();
			if (this.playerLost()) {
				this.gameOver();
			} else {
				this.moveSnake();
			}
		}
	}

	turnLeft() {
		this.snakeTurn = SNAKE_TURN_LEFT;
	}

	turnRight() {
		this.snakeTurn = SNAKE_TURN_RIGHT;
	}

	updateDirection() {
		if (this.snakeTurn === SNAKE_TURN_LEFT) {
			switch (this.snakeDirection) {
				case SNAKE_DIR_UP:
					this.snakeDirection = SNAKE_DIR_LEFT;
					break;
				case SNAKE_DIR_DOWN:
					this.snakeDirection = SNAKE_DIR_RIGHT;
					break;
				case SNAKE_DIR_LEFT:
					this.snakeDirection = SNAKE_DIR_DOWN;
					break;
				case SNAKE_DIR_RIGHT:
					this.snakeDirection = SNAKE_DIR_UP;
					break;
			}
		} else if (this.snakeTurn === SNAKE_TURN_RIGHT) {
			switch (this.snakeDirection) {
				case SNAKE_DIR_UP:
					this.snakeDirection = SNAKE_DIR_RIGHT;
					break;
				case SNAKE_DIR_DOWN:
					this.snakeDirection = SNAKE_DIR_LEFT;
					break;
				case SNAKE_DIR_LEFT:
					this.snakeDirection = SNAKE_DIR_UP;
					break;
				case SNAKE_DIR_RIGHT:
					this.snakeDirection = SNAKE_DIR_DOWN;
					break;
			}
		}
		this.snakeTurn = SNAKE_NO_DIR_TURN;
	}

	calcNextTile() {
		this.snakeHeadNext_x = this.snakeHead_x;
		this.snakeHeadNext_y = this.snakeHead_y;

		switch(this.snakeDirection) {
			case SNAKE_DIR_UP:
				this.snakeHeadNext_y -= 1;
				break;
			case SNAKE_DIR_DOWN:
				this.snakeHeadNext_y += 1;
				break;
			case SNAKE_DIR_LEFT:
				this.snakeHeadNext_x -= 1;
				break;
			case SNAKE_DIR_RIGHT:
				this.snakeHeadNext_x += 1;
				break;
		}
	}

	playerLost() {
		if (this.snakeHeadNext_x < 0 || this.snakeHeadNext_x >= this.screenWidth
			|| this.snakeHeadNext_y < 0 || this.snakeHeadNext_y >= this.screenHeight) {
			return true;
		}
		let tile = this.screenMatrix[this.snakeHeadNext_y][this.snakeDirection];
		return tile > 0 && tile !== this.tailTile;
	}

	gameOver() {
		alert('Game Over');
		this.state = STATE_STOPPED;
	}

	moveSnake() {
		for (let i = 0; i < this.screenMatrix.length; i++) {
			for (let j = 0; j < this.screenMatrix[i].length; j++) {
				if (this.screenMatrix[i][j] == this.tailTile) {
					this.screenMatrix[i][j] = 0;
				} else if (this.screenMatrix[i][j] > 0) {
					this.screenMatrix[i][j]++;
				}
			}
		}
		this.screenMatrix[this.snakeHeadNext_y][this.snakeHeadNext_x] = 1;
		this.snakeHead_x = this.snakeHeadNext_x;
		this.snakeHead_y = this.snakeHeadNext_y;
	}

	drawScreen() {
		let screen = '|' + '-'.repeat(this.screenWidth);
		for (let i = 0; i < this.screenMatrix.length; i++) {
			screen += '|\n|';
			for (let j = 0; j < this.screenMatrix[i].length; j++) {
				let tile = this.screenMatrix[i][j];
				if (tile == 0) {
					screen += ' ';	
				} else if (tile < 0) {
					screen += '%';
				} else {
					screen += 'O'
				}	
			}
		}
		screen += '|\n|' + '-'.repeat(this.screenWidth) + '|';
		$('#snake-screen').text(screen);
	}


}
