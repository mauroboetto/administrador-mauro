const BOARD_WIDTH = 25;
const BOARD_HEIGHT = 15;

const DIRECTION_LEFT = 0;
const DIRECTION_RIGHT = 1;
const DIRECTION_UP = 2;
const DIRECTION_DOWN = 3;

const GAME_STATE_PLAYING = 0;
const GAME_STATE_LOST = 1;
const GAME_STATE_STAND_BY = 2;
const GAME_STATE_PAUSED = 3;

const ARROW_KEY_LEFT = 37;
const ARROW_KEY_UP = 38;
const ARROW_KEY_RIGHT = 39;
const ARROW_KEY_DOWN = 40;
const ENTER_KEY = 13;

class SnakeBoard extends React.Component {
    constructor(props) {
        super(props);
        this.state = this.newGame(GAME_STATE_STAND_BY);
        this.state.funcHandleKeyDown = (e) => this.handleKeyDown(e);
        setTimeout(() => this.mainLoop(), 0);
    }

    newGame(game_state) {
        let board = Array(BOARD_HEIGHT);
        for (let i = 0; i < BOARD_HEIGHT; i++) {
            board[i] = Array(BOARD_WIDTH).fill(0);
        }

        let mid_y = Math.floor(BOARD_HEIGHT / 2);
        let mid_x = Math.floor(BOARD_WIDTH / 2);
        board[mid_y][mid_x - 1] = 3;
        board[mid_y][mid_x] = 2;
        board[mid_y][mid_x + 1] = 1;
        this.generateFood(board);
        let state = {
            board: board,
            direction: DIRECTION_LEFT,
            head_x: mid_x - 1,
            head_y: mid_y,
            head_value: 3,
            game_state: game_state,
            score: 0,
        };

        return state;
    }

    mainLoop() {
        if (this.state.game_state === GAME_STATE_PLAYING) {
            this.updateBoard();
        }
        let time_step = 250 - 20 * this.state.score;
        if (time_step < 50) {
            time_step = 50;
        }
        setTimeout(() => this.mainLoop(), time_step);
    }

    componentDidMount() {
        document.addEventListener('keydown', this.state.funcHandleKeyDown, false);
    }
    componentWillUnmount() {
        document.removeEventListener('keydown', this.state.funcHandleKeyDown, false);
    }
    handleKeyDown(event) {
        const game_state = this.state.game_state;
        if (game_state === GAME_STATE_PLAYING) {
            let new_direction = null;
            let current_direction = this.state.direction;
            switch (event.keyCode) {
                case ARROW_KEY_DOWN:
                    if (current_direction === DIRECTION_LEFT || current_direction === DIRECTION_RIGHT)
                        new_direction = DIRECTION_DOWN;
                    break;
                case ARROW_KEY_UP:
                    if (current_direction === DIRECTION_LEFT || current_direction === DIRECTION_RIGHT)
                        new_direction = DIRECTION_UP;
                    break;
                case ARROW_KEY_LEFT:
                    if (current_direction === DIRECTION_UP || current_direction === DIRECTION_DOWN)
                        new_direction = DIRECTION_LEFT;
                    break;
                case ARROW_KEY_RIGHT:
                    if (current_direction === DIRECTION_UP || current_direction === DIRECTION_DOWN)
                        new_direction = DIRECTION_RIGHT;
                    break;
                case ENTER_KEY:
                    this.setState({game_state: GAME_STATE_PAUSED});
            }
            if (new_direction !== null) {
                this.setState({direction: new_direction});
            }
        } else if (game_state === GAME_STATE_LOST) {
            if (event.keyCode === ENTER_KEY) this.setState(this.newGame(GAME_STATE_PLAYING));
        } else if (game_state === GAME_STATE_STAND_BY) {
            if (event.keyCode === ENTER_KEY) this.setState({game_state: GAME_STATE_PLAYING});
        } else if (game_state === GAME_STATE_PAUSED) {
            if (event.keyCode === ENTER_KEY) this.setState({game_state: GAME_STATE_PLAYING});
        }
        
    }

    generateFood(board) {
        let n_loops = 0;
        let initial_i = Math.floor(Math.random() * BOARD_HEIGHT);
        let initial_j = Math.floor(Math.random() * BOARD_WIDTH);
        while(n_loops < 2) {
            for (let i = initial_i; i < BOARD_HEIGHT; i++) {
                for (let j = initial_j; j < BOARD_WIDTH; j++) {
                    if (board[i][j] === 0) {
                        board[i][j] = TILE_FOOD;
                        return true;
                    }
                }
                initial_j = 0;
            }
            initial_i = 0;
            n_loops++;
        }
        return false;
    };

    updateBoard() {
        let board = this.state.board;
        const current_head_x = this.state.head_x;
        const current_head_y = this.state.head_y;
        let next_head_x, next_head_y;
        switch (this.state.direction) {
            case DIRECTION_DOWN:
                next_head_x = current_head_x;
                next_head_y = current_head_y + 1;
                break;
            case DIRECTION_UP:
                next_head_x = current_head_x;
                next_head_y = current_head_y - 1;
                break;
            case DIRECTION_RIGHT:
                next_head_x = current_head_x + 1;
                next_head_y = current_head_y;
                break;
            case DIRECTION_LEFT:
                next_head_x = current_head_x - 1;
                next_head_y = current_head_y;
                break;
            default:
                console.log('default shouldn\'t be reached')
        }
        if (next_head_x >= 0 && next_head_y >= 0 && next_head_x < BOARD_WIDTH && next_head_y < BOARD_HEIGHT) {
            // Check if there is food
            if (board[next_head_y][next_head_x] === TILE_FOOD) {
                this.setState({head_value: this.state.head_value + 1, score: this.state.score + 1});
                this.generateFood(board);
            } else if (board[next_head_y][next_head_x] > 0) {
                this.playerLost();
            } else {
                // Remove tail
                for (let i = 0; i < BOARD_HEIGHT; i++) {
                    for (let j = 0; j < BOARD_WIDTH; j++) {
                        if (board[i][j] > 0) {
                            board[i][j]--;
                        }
                    }
                }
            }
            // Draw head
            board[next_head_y][next_head_x] = this.state.head_value;
        } else {
            this.playerLost();
        }

        this.setState({
            board: board,
            head_x: next_head_x,
            head_y: next_head_y,
        });
    }

    playerLost() {
        this.setState({game_state: GAME_STATE_LOST});
    }

    thereIsAMessage() {
        return this.state.game_state !== GAME_STATE_PLAYING;
    }

    getMessage() {
        switch (this.state.game_state) {
            case GAME_STATE_LOST:
                return 'GAME OVER';
            case GAME_STATE_PAUSED:
                return 'PAUSE';
            case GAME_STATE_STAND_BY:
                return 'PRESS START';
        }
        return '';
    }

    getRowsComponents() {
        let rows = [];
        for (let i = 0; i < this.state.board.length; i++) {
            rows.push(<SnakeBoardRow key={'snake-row-' + i} tiles_array={this.state.board[i]} index_y={i}/>);
        }
        return rows;
    }

    render() {
        return (
            <div>
                <h2>Score: {this.state.score}</h2>
                <div className="snake-board">
                    <div className={'snake-board-message' + (!this.thereIsAMessage() ? ' hidden' : '')}>
                        <table><tr><td>{this.getMessage()}</td></tr></table>
                    </div>
                    {this.getRowsComponents()}
                </div>
            </div>
        )
    }
    
}

class SnakeBoardRow extends React.Component {
    tileContainsSnake(tiles_array, tile_index) {
        return tiles_array[tile_index] > 0;
    }

    render() {
        let tiles = [];
        for (let i = 0; i < BOARD_WIDTH; i++) {
            tiles.push(
                <SnakeBoardTile contains_snake={this.tileContainsSnake(this.props.tiles_array, i)} 
                    index_y={this.props.index_y} index_x={i} tile_value={this.props.tiles_array[i]}/>
            );
        }
        return (
            <div className="snake-board-row">
                {tiles}
            </div>
            
        );
    }
}

const TILE_FOOD = -1;
class SnakeBoardTile extends React.Component {
    getTileClass(tile_value) {
        if (tile_value > 0) {
            return 'contains-snake'
        } else if (tile_value === TILE_FOOD) {
            return 'contains-food';
        }

        return '';
    }
    render() {
        return (
            <div className={'snake-board-tile ' + this.getTileClass(this.props.tile_value)}>
                 -
            </div>
        );
    }
}
